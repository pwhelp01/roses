%
% Programminging Language Paradigms (UG) Coursework 2015/2016
% Prolog Assignment
% Submitted by: Pete Whelpton (pwhelp01 / 12828513)
% Lecturers: Keith Mannock / Trevor Fenner
% Due date: 1st April 2016
%

% Revision History
% 22/11/2015 - Initial Commit, not working :(  No errors, but doesn't find a solution
% 22/11/2015 - Fixed typos ('rose' instead of 'leroy', lines 70 & 155).  Solves puzzle!  47s on my i3 laptop.  Can this be optimized?
% 19/03/2016 - Removed person() facts, because they are actually superfluous to the puzzle

% Solution shamelessly based on the "It's a tie" example %

 
% Facts %
% Rose facts
rose(golden-sunset).
rose(cottage-beauty).
rose(pink-paradise).
rose(sweet-dreams).
rose(mountain-bloom).

% Event facts
event(senior-prom).
event(wedding).
event(charity-auction).
event(anniversary-party).
event(retirement-banquet).

% Item facts
item(place-cards).
item(streamers).
item(balloons).
item(gourmet-chocolates).
item(candles).


 
% Main Solution %
solve :- 	% Person / Rose relations
			rose(IdaRose), rose(JeremyRose), rose(StellaRose), rose(HughRose), rose(LeroyRose),
			% Create a list of teacher/subject relations and ensure they are all different
			all_different([IdaRose, JeremyRose, StellaRose, HughRose, LeroyRose]),

			% Person / Event relations
			event(IdaEvent), event(JeremyEvent), event(StellaEvent), event(HughEvent), event(LeroyEvent),
			% Create a list of teacher/County relations and ensure they are all different
			all_different([IdaEvent, JeremyEvent, StellaEvent, HughEvent, LeroyEvent]),
			
			% Person / Item relations
			item(IdaItem), item(JeremyItem), item(StellaItem), item(HughItem), item(LeroyItem),
			% Create a list of teacher/Activity relations and ensure they are all different
			all_different([IdaItem, JeremyItem, StellaItem, HughItem, LeroyItem]),
			
			
			% Create a list of Holidays consisting of lists for each teacher
			Purchases = [
							[ida, IdaRose, IdaEvent, IdaItem],
							[jeremy, JeremyRose, JeremyEvent, JeremyItem],
							[stella, StellaRose, StellaEvent, StellaItem],
							[hugh, HughRose, HughEvent, HughItem],
							[leroy, LeroyRose, LeroyEvent, LeroyItem]
						],
			
			
			/* Clues */
			% 1.  Jeremy made a purchase for the senior prom.  
			member([jeremy, _, senior-prom, _], Purchases),
			
			% Stella (who didn't choose flowers for a wedding) picked the Cottage Beauty variety. 
			\+ member([stella, _, wedding, _], Purchases),
			member([stella, cottage-beauty, _, _], Purchases),
			
			
			% 2. Hugh (who selected the Pink Paradise blooms) 
			member([hugh, pink-paradise, _, _], Purchases),
			
			% didn't choose flowers for either the charity auction or the wedding.
			\+ member([hugh, _, charity-auction, _], Purchases),
			\+ member([hugh, _, wedding, _], Purchases),
			
			
			% 3. The  customer  who  picked  roses  for  an  anniversary  party  also  bought  streamers.
			member([_, _, anniversary-party, streamers], Purchases),
			
			% The one shopping for a wedding chose the balloons.
			member([_, _, wedding, balloons], Purchases),
			
			
			% 4.  The  customer  who  bought  the  Sweet  Dreams  variety  also  bought  gourmet  chocolates.  
			member([_, sweet-dreams, _, gourmet-chocolates], Purchases),
			
			% Jeremy didn't pick the Mountain Bloom variety.
			\+ member([jeremy, mountain-bloom, _, _], Purchases),
			
			% Leroy was shopping for the retirement banquet.  
			member([leroy, _, retirement-banquet, _], Purchases),
			 
			% The customer in charge of decorating the senior prom also bought the candles
			member([_, _, senior-prom, candles], Purchases),
			
			% Display results %
			tell(ida, IdaRose, IdaEvent, IdaItem),
			tell(jeremy, JeremyRose, JeremyEvent, JeremyItem),
			tell(stella, StellaRose, StellaEvent, StellaItem),
			tell(hugh, HughRose, HughEvent, HughItem),
			tell(leroy, LeroyRose, LeroyEvent, LeroyItem).
			
% "Borrowed" from the example
% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

% "Borrowed" from the example
tell(P, R, E, I) :- write(P), write(' who bought '), write(R), write(' is going to the '), write(E), write(' and also bought '), write(I), nl.
			